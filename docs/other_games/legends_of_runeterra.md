# Legends of Runeterra

* 🖥️  There is a [dedicated LoR Lutris script](https://lutris.net/games/legends-of-runeterra/), to install:
 1. Follow steps 1-3 in [Installing League of Legends via Lutris]().
 2. Download and install the Lutris script titled `No Disconnect version`.
* 📱 Available on [Android](https://play.google.com/store/apps/details?id=com.riotgames.legendsofruneterra) and [iOS](https://apps.apple.com/app/id1480617557).
* Elsewhere on reddit at r/legendsofruneterra.