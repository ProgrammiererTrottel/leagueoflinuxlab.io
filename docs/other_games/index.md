# 🕹️ Riot Games Catalogue

Besides League of Legends, Riot and their partners also develop a number of other games for a variety of platforms. In addition to developing their own games, Riot also publishes a number of games developed by other studios using the League of Legends IP via their publishing label Riot Forge. It is possible to play the Android versions of games on desktop via a couple different methods.

## Riot Games

* [Teamfight Tactics](teamfight_tactics.md)
* [Legends of Runeterra](legends_of_runeterra.md)
* [VALORANT](valorant.md)
* [Wild Rift](wild_rift.md)
* [Project L](project_l.md)

## Riot Forge

* [Ruined King: A League of Legends Story](ruined_king.md)
* [Hextech Mayhem: A League of Legends Story](hextech_mayhem.md)
* [The Mageseeker: A League of Legend Story](the_mageseeker.md)
* [CONV/RGENCE: A League of Legends Story](convergence.md)
* [Song of Nunu: A League of Legends Story](song_of_nunu.md)
