# How to Capture Verbose Logs

## Why do we Need Verbose Logs?

By default the logging enabled in Wine and Lutris builds do not collect enough detailed information to solve many issues. This is in fact usually a good thing as Wine logs can get very densely complicated very quickly, so less logging means less resources used when playing games. Fantastic when things are working correctly, not so great when trying to troubleshoot technical problems. 

## Lutris Logs

To capture more verbose logs in Lutris if you already have League installed:

1. Right click League of Legends
2. Select `Configure`
3. Select `Runner options`
4. Enable `Output debugging info` 
5. Close Lutris completely
6. Open your terminal and run `lutris -d`
7. Try to reproduce your issue

To capture more verbose logs in Lutris if you _don't_ already have League installed (ie. you're having problems installing League):

1. Close Lutris entirely
2. Open your terminal and run `lutris -d`
3. Try to reproduce your issue

After you've reproduced your issue you need to copy the output in the terminal and share all of it with us. Do not try to past the logs directly into a reddit post as reddit will not format the large amount of text nicely. Instead you can use a pastebin service such as [Ubuntu's Pastebin](https://paste.ubuntu.com/).


## Wine Logs

If you are not using Lutris you can manually change [Wine's Debug Channels](https://wiki.winehq.org/DebugChannels) to capture verbose logs. For example: using the `WINEDEBUG=warn+all` when launching your desired `.exe`. For more information please read the [WineHQ wiki](https://wiki.winehq.org/Wine_Developer%27s_Guide/Debug_Logging).
